#le bootstrap

Vous créez des pages web et vous passez beaucoup (trop) de temps avec le CSS ?

Alors Bootstrap est fait pour vous !

Ce cours va vous guider dans la découverte de cette puissante boîte à outils. Bootstrap, kit CSS créé par les développeurs de Twitter, est devenu en peu de temps le framework CSS de référence. Vous allez découvrir pas à pas comment construire rapidement et facilement des sites web esthétiques et responsives. Bootstrap offre aussi des plugins jQuery de qualité pour enrichir vos pages.

Pour profiter pleinement de ce cours, il faudrait que vous possédiez quelques connaissances préalables :

La base indispensable est de posséder de bonnes notions en HTML et CSS. Vous avez des lacunes ? Comblez-les avec le cours de Mathieu Nebra.

Pour comprendre la mise en œuvre des plugins jQuery, vous aurez besoin des quelques bases dans ce domaine. Il vous en manque ? Alors vous pouvez réparer ça en lisant le cours de Michel Martin.


Avant toute chose, il faut définir ce qu'est un framework. Il s'agit d'un ensemble de composants structurés qui sert à créer les bases et à organiser le code informatique pour faciliter le travail des programmeurs, que ce soit en terme de productivité ou de simplification de la maintenance. Il en existe beaucoup pour les applications web qui ciblent de nombreux langages : Java, Python, Ruby, PHP…

Il existe des frameworks côté serveur (désignés backend en anglais), et d'autres côté client (désignés frontend en anglais). Bootstrap fait partie de cette deuxième catégorie. Les frameworks CSS sont spécialisés, comme leur nom l'indique, dans les CSS ! C'est-à-dire qu'ils nous aident à mettre en forme les pages web : organisation, aspect, animation… Ils sont devenus à la mode et il en existe un certain nombre :

-Knacss

-Blueprint

-unsemantic

-YUI

-BlueTrip

-ez-css

-Pure

-Gumby

-Materialize

-etc.

Bootstrap est un framework CSS, mais pas seulement, puisqu'il embarque également des composants HTML et JavaScript. Il comporte un système de grille simple et efficace pour mettre en ordre l'aspect visuel d'une page web. Il apporte du style pour les boutons, les formulaires, la navigation… Il permet ainsi de concevoir un site web rapidement et avec peu de lignes de code ajoutées. Le framework le plus proche de Bootstrap est sans doute Foundation qui est présenté comme « The most advanced responsive front-end framework in the world ». Cette absence de modestie est-elle de mise ? Je pense que c'est surtout une affaire de goût et de préférence personnelle. En tout cas en terme de popularité c'est Bootstrap qui l'emporte haut la main.


Bootstrap propose :

Une mise en page basée sur une grille de 12 colonnes bien pratique. Bien sûr, si vous avez besoin de plus de 12 colonnes, ou de moins, il est toujours possible de changer la configuration ;

L'utilisation de Normalize.css ;

Du code fondé sur HTML5 et CSS3 ;

Une bibliothèque totalement open source sous license MIT;

Du code qui tient compte du format d'affichage des principaux outils de navigation (responsive design) : smartphones, tablettes… ;

Des plugins jQuery de qualité ;

Un résultat cross-browser (la prise en charge de IE7 a été abandonnée avec la version 3), donc une garantie de compatibilité maximale ;

Une bonne documentation ;

La garantie d'une évolution permanente ;

Une mine de ressources variées sur le web ;

Une architecture basée sur LESS, un outil bien pratique qui étend les possibilités de CSS (un portage sur SASS existe également).

Vous pouvez trouver toutes les informations sur Bootstrap directement sur le site dédié.

























