#le markdown 
Markdown, ça vous dit quelque chose ?
C'est un petit langage très simple qui permet d'écrire du HTML de façon raccourcie. On peut s'en servir sur certains forums (Le Site du Zéro, Stack Overflow...) ou pour rédiger des documentations (incontournable sur GitHub).

Les créateurs de Markdown trouvaient que ce n'était pas vraiment pratique de devoir mettre en forme manuellement des textes en HTML (ou, pire, en bbCode). Imaginez, vous voulez poster un message sur un forum qui accepte uniquement le HTML et vous voulez écrire une liste à puces. Vous allez devoir écrire à la main…

```html
<ul>
<li>Le contenu de ma première puce</li>
<li>Le contenu de ma seconde puce</li>
<li>Et encore une autre puce</li>
</ul>
```

… et ça, juste pour faire une liste à puces, il faut avouer que ça devient vite lourdingue !

John Gruber et Aaron Swartz ont donc créé un petit langage très simple appelé le Markdown. L'idée est de pouvoir mettre en forme du texte sans avoir besoin de recourir à la souris… et sans avoir besoin de taper à la main des balises HTML toutes les 5 secondes.

Voici un exemple de texte rédigé en Markdown et son résultat à l'affichage pour vous donner une idée de la simplicité du langage :                                                                                                                                                                                                                                                         
             je suis un texte rédigé en ** Markdown ** -----------> je suis un texte rédigé en **Markdown**

          texte en Markdown                                       Résulat affiché        














